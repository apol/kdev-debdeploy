/*
 * Copyright 2015 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include "packagejob.h"
#include <QTextStream>
#include <KLocalizedString>
#include <interfaces/icore.h>
#include <interfaces/iruncontroller.h>
#include <interfaces/iproject.h>
#include <QTimer>

using namespace KDevelop;

PackageJob::PackageJob(IProject* project, QObject* parent)
    : KJob(parent)
    , m_project(project)
    , m_dir(QDir::tempPath() + "/deploy-" + project->name())
{
    connect(&m_proc, static_cast<void(QProcess::*)(int)>(&QProcess::finished), this, &PackageJob::packagingFinished);
    connect(&m_proc, static_cast<void(QProcess::*)(QProcess::ProcessError)>(&QProcess::error), this, &PackageJob::packagingFailed);
    Q_ASSERT(m_dir.isValid());
}

QString PackageJob::fileName() const
{
    return m_project->name() + ".deb";
}

QString PackageJob::destinationPath() const
{
    return outputDir().absoluteFilePath(fileName());
}

void PackageJob::start()
{
    QTimer::singleShot(0, this, &PackageJob::doStart);
}

void PackageJob::doStart()
{
    bool b = QDir::temp().mkpath(outputDir().absoluteFilePath("DEBIAN"));
    Q_ASSERT(b);

    QFile data(outputDir().absoluteFilePath("DEBIAN/control"));

    if (!data.open(QFile::WriteOnly | QFile::Truncate)) {
        setError(4);
        setErrorText(i18n("Couldn't open the %1 file", data.fileName()));
        emitResult();
        return;
    }

    QString pkgName;

    QTextStream out(&data);
    out << "Package: " << pkgName << endl
        << "Version: 999:999" << endl
        << "Architecture: " << qgetenv("DEB_HOST_ARCH") << endl
        << "Maintainer: KDevelop <invalid@kde.org>" << endl
        << "Description: fake package developed by a lazy developer." << endl;
    data.close();

    m_proc.setProgram(QStringLiteral("dpkg-deb"));
    m_proc.setArguments(QStringList { QStringLiteral("-b"), prefixPath().toLocalFile(), destinationPath()});
    m_proc.start();
}

void PackageJob::packagingFinished(int exitCode)
{
    setError(exitCode);
    emitResult();
}

void PackageJob::packagingFailed(QProcess::ProcessError error)
{
    setError(255 + error);
    setErrorText(m_proc.errorString());
    emitResult();
}

QUrl PackageJob::prefixPath() const
{
    return QUrl::fromLocalFile(outputDir().absoluteFilePath(QStringLiteral("prefix")));
}

QDir PackageJob::outputDir() const
{
    return QDir(m_dir.path());
}
