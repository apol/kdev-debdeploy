/*
 * Copyright 2015 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#ifndef PACKAGEJOB_H
#define PACKAGEJOB_H

#include <KJob>
#include <QTemporaryDir>
#include <QProcess>
#include <interfaces/iproject.h>

class PackageJob : public KJob
{
Q_OBJECT
public:
    PackageJob(KDevelop::IProject* project, QObject* parent = nullptr);
    void start() override;

    QString fileName() const;
    QString destinationPath() const;
    QUrl prefixPath() const;

private:
    void doStart();
    QDir outputDir() const;
    void performPackaging();
    void packagingFinished(int exitCode);
    void packagingFailed(QProcess::ProcessError error);

    KDevelop::IProject* m_project;
    QTemporaryDir m_dir;
    QProcess m_proc;
};

#endif // PACKAGEJOB_H
