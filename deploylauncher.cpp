/*
 * Copyright 2015 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */

#include <interfaces/icore.h>
#include <interfaces/iruncontroller.h>
#include <interfaces/ilaunchconfiguration.h>
#include <interfaces/iuicontroller.h>
#include <interfaces/iplugincontroller.h>
#include <interfaces/iproject.h>
#include <project/projectmodel.h>
#include <project/interfaces/ibuildsystemmanager.h>
#include <project/interfaces/iprojectbuilder.h>
#include <outputview/outputexecutejob.h>
#include <util/executecompositejob.h>
#include <KIO/CopyJob>

#include <kdevelop/make/imakebuilder.h>

#include <KLocalizedString>
#include <QDebug>
#include <QRegularExpression>

#include <execute/iexecuteplugin.h>

#include "deploylauncher.h"
#include "packagejob.h"

static KJob* fetchInstallJob(KDevelop::IProject* m_project, const QUrl &prefixPath)
{
    if (!m_project->buildSystemManager() || !m_project->buildSystemManager()->builder()) {
        return nullptr;
    }

    KJob* j = m_project->buildSystemManager()->builder()->install(m_project->projectItem(), prefixPath);
    if (!j) {
        return nullptr;
    }
    return j;
}

static QList<KJob*> packageDeb(KDevelop::ILaunchConfiguration* cfg)
{
    IExecutePlugin* iface = KDevelop::ICore::self()->pluginController()->pluginForExtension(QStringLiteral("org.kdevelop.IExecutePlugin"))->extension<IExecutePlugin>();
    Q_ASSERT(iface);

    PackageJob* pkgjob = new PackageJob(cfg->project());
    KJob* installJob = fetchInstallJob(cfg->project(), pkgjob->prefixPath());

    QString error;

    const QString remoteDeb = "/tmp/" + pkgjob->fileName();
    KDevelop::OutputExecuteJob* pushjob = new KDevelop::OutputExecuteJob;
    *pushjob << QStringList{ "push", pkgjob->destinationPath(), remoteDeb };
    KDevelop::OutputExecuteJob* installjob = new KDevelop::OutputExecuteJob;
    *installjob << QStringList{ "shell", "dpkg", "-i", remoteDeb } ;
    KDevelop::OutputExecuteJob* executejob = new KDevelop::OutputExecuteJob;
    *executejob << QStringList{ "shell", iface->executable(cfg, error).fileName() };
    *executejob << iface->arguments(cfg, error);

    QList<KJob*> ret = { installJob, pkgjob, pushjob, installjob, executejob };
    if (!error.isEmpty()) {
        qDeleteAll(ret);
        qWarning() << "error: " << error;
        ret.clear();
    }

    return ret;
}

QString cmakeCacheValue(const KDevelop::Path &path, const QByteArray& varname)
{
    QFile f(path.toLocalFile());
    if (!f.open(QIODevice::ReadOnly)) {
        return {};
    }

    QTextStream stream(&f);

    QRegularExpression rx("^"+varname+":?.*=(.*)$");

    for(QString line = stream.readLine(); stream.atEnd(); line = stream.readLine()) {
        QRegularExpressionMatch match = rx.match(line);
        if (match.isValid()) {
            return match.captured(1);
        }
    }

    return {};
}

static QList<KJob*> createEcmAndroidApk(KDevelop::ILaunchConfiguration* cfg)
{
    KDevelop::IProject* p = cfg->project();

    IMakeBuilder* imb = dynamic_cast<IMakeBuilder*>(p->buildSystemManager()->builder());
    if (!imb) {
        return {};
    }
    KJob * createApk = imb->executeMakeTarget(p->projectItem(), "create-apk");

    KDevelop::Path builddir = p->buildSystemManager()->buildDirectory(p->projectItem());
    KDevelop::Path apk(builddir, cmakeCacheValue(builddir, "QTANDROID_EXPORTED_TARGET") + QLatin1String("_build_apk/bin/QtApp-debug.apk"));

    KDevelop::OutputExecuteJob* installjob = new KDevelop::OutputExecuteJob;
    *installjob << QStringList{ "install", "-r", apk.toLocalFile() };

    return { createApk, installjob };
}

static QList<KJob*> sendSources(KDevelop::ILaunchConfiguration* cfg)
{
    qDebug() << "xxxxxxxxxx" << cfg->project()->path().toUrl();
    return { KIO::copy(cfg->project()->path().toUrl(), QUrl::fromLocalFile("/tmp")) };
}

static const QHash<QString, std::function<QList<KJob*>(KDevelop::ILaunchConfiguration*)>> s_integrations = {
    {QStringLiteral("debadbdeploy"), packageDeb },
    {QStringLiteral("androiddeploy"), createEcmAndroidApk },
    {QStringLiteral("sendSources"), sendSources }
};

DeployLauncher::DeployLauncher(DeployPlugin *inst)
{
}

KJob* DeployLauncher::start(const QString& launchMode, KDevelop::ILaunchConfiguration* cfg)
{
    Q_ASSERT(cfg);
    if (!cfg)
        return nullptr;

    auto func = s_integrations.value(launchMode);

    if (func) {
        QList<KJob*> jobs = func(cfg);
        if (jobs.isEmpty())
            return nullptr;

        KJob* j = new KDevelop::ExecuteCompositeJob(KDevelop::ICore::self()->runController(), jobs);
        j->setObjectName(i18n("Deploying %1", cfg->name()));
        return j;
    } else {
        qWarning() << "Unknown launch mode " << launchMode << "for config:" << cfg->name();
    }
    return nullptr;
}

QStringList DeployLauncher::supportedModes() const
{
    return s_integrations.keys();
}

QList<KDevelop::LaunchConfigurationPageFactory*> DeployLauncher::configPages() const
{
    return {};
}

QString DeployLauncher::description() const
{
    return i18n("Deploy into a device...");
}

QString DeployLauncher::id()
{
    return QStringLiteral("deploy");
}

QString DeployLauncher::name() const
{
    return i18n("Deploy");
}
