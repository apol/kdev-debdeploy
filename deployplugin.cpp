/* This file is part of KDevelop
 *  Copyright 2002 Harald Fernengel <harry@kdevelop.org>
 *  Copyright 2007 Hamish Rodda <rodda@kde.org>

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; see the file COPYING.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

 */

#include <QLabel>
#include <QAction>

#include <kactioncollection.h>
#include <klocalizedstring.h>
#include <kpluginfactory.h>
#include <kpluginloader.h>
#include <kaboutdata.h>

#include <execute/iexecuteplugin.h>

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>
#include <interfaces/iruncontroller.h>
#include <interfaces/launchconfigurationtype.h>
#include <interfaces/iplugincontroller.h>
#include "deployplugin.h"
#include "deploylauncher.h"

#include <QDebug>

using namespace KDevelop;

K_PLUGIN_FACTORY_WITH_JSON(DeployFactory, "kdev-deploy.json",  registerPlugin<DeployPlugin>();)

class DeployWidgetFactory : public IToolViewFactory
{
public:
    DeployWidgetFactory(DeployPlugin* /*plugin*/)
    {}

    QWidget* create(QWidget *parent = 0) override {
        return new QLabel(parent);
    }

    Qt::DockWidgetArea defaultPosition() override {
        return Qt::BottomDockWidgetArea;
    }

    QString id() const override {
        return QStringLiteral("org.kdevelop.DeployView");
    }
};

DeployPlugin::DeployPlugin(QObject *parent, const QVariantList&)
    : IPlugin("kdev-deploy", parent)
    , m_factory(new DeployWidgetFactory(this))
{
    core()->uiController()->addToolView(i18n("Deploy"), m_factory);

    QAction* act = actionCollection()->addAction(QStringLiteral("run-deploy"), this, SLOT(runDeploy()));
    act->setStatusTip(i18n("Launches the currently selected launch configuration with the Deploy presets"));
    act->setText(i18n("Deploy launch"));

    actionCollection()->addAction( "project_deploy", act );

    auto pc = ICore::self()->pluginController();
    IExecutePlugin* iface = pc->pluginForExtension(QStringLiteral("org.kdevelop.IExecutePlugin"), "kdevexecute")->extension<IExecutePlugin>();
    Q_ASSERT(iface);

    LaunchConfigurationType* type = core()->runController()->launchConfigurationTypeForId(iface->nativeAppConfigTypeId());
    Q_ASSERT(type);

    type->addLauncher(new DeployLauncher(this));

    setXMLFile("kdev-deploy.rc");
}

DeployPlugin::~DeployPlugin()
{
}

void DeployPlugin::unload()
{
    core()->uiController()->removeToolView(m_factory);
}

void DeployPlugin::runDeploy()
{
    core()->runController()->executeDefaultLaunch(QStringLiteral("sendSources"));
}

#include "deployplugin.moc"
